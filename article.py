class article(object):
    """
        Attributs :
        title (str) : contient le nom de l'article
        category (str) : contient le catégorie de l'article
    """   
    def __init__(self, title, category):
        self._title = title
        self._category = category

    @property
    def title(self):
        """Renvoie le titre de l'article"""
        return self._title

    @title.setter
    def title(self, title):
        """Modifie le titre de l'article"""
        self._title = title

    @property
    def category(self):
        """Renvoie la catégorie de l'article"""
        return self._category

    @category.setter
    def category(self, category):
        """Modifie la catégorie de l'article"""
        self._category = category
