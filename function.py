from article import *

def multiply(a, b):
    """Renvoie le produit de a et de b"""
    pass

def title_article(article):
    """Renvoie le titre de l'article"""
    pass

def change_name_article(article, new_title):
    """Change le titre de l'article avec le nouveau"""
    pass

def add_article_to_list(article, articles):
    """Ajoute un article à la liste des articles par effet de bord"""
    pass

def remove_article_to_list(articles):
    """Supprime un article de la liste des articles par effet de bord"""
    pass

def second_article(articles):
    """Renvoie le second article de la liste"""
    pass

def articles_with_category(articles, category):
    """Renvoie la liste des articles de la catégorie passé en paramètre"""
    pass

def articles_with_two_categories(articles, category, category2):
    """Renvoie la liste des articles des catégories passées en paramètre"""
    pass

def display_articles_if_more_one(articles):
    """Renvoie Vrai s'il y a plus d'un article dans la liste sinon Faux"""
    pass

def count_articles(articles):
    pass
